package com.userfront.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by snowwhite on 6/2/2017.
 */
@Data
@Entity
public class SavingsTransaction {
	@Id
	@GeneratedValue
	private Long id;
	private Date date;
	private String description;
	private String type;
	private String status;
	private double amount;
	private BigDecimal availableBalance;

	@ManyToOne
	@JoinColumn(name="savings_account_id")
	private SavingsAccount savingsAccount;

	public SavingsTransaction(Date date, String description, String type, String status, double amount,
	                          BigDecimal availableBalance, SavingsAccount savingsAccount) {
		super();
		this.date = date;
		this.description = description;
		this.type = type;
		this.status = status;
		this.amount = amount;
		this.availableBalance = availableBalance;
		this.savingsAccount = savingsAccount;
	}
}
