package com.userfront.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by snowwhite on 6/2/2017.
 */
@Data
@Entity
public class Appointment {

	@Id
	@GeneratedValue
	private Long id;
	private Date date;
	private String location;
	private String description;
	private boolean confirmed;

	@ManyToOne //relationship from appointment to user is many to one
	//One user can have many appointments
	@JoinColumn(name="user_id") //Primary key of user
	private User user;
}
