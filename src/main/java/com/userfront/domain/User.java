package com.userfront.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * Created by snowwhite on 6/2/2017.
 */
@Data
@Entity
public class User {
	@Id
	@GeneratedValue
	@Column(name="userId", nullable=false, updatable = false)
	private long userID;
	private String username;
	private String password;
	private String firstname;
	private String lastname;

	@Column(name="email", nullable = false, unique=true)
	private String email;
	private String phone;

	private boolean enabled=true; //User is enabled or not

	@OneToOne
	private PrimaryAccount primaryAccount;

	@OneToOne
	private SavingsAccount savingsAccount;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Appointment> appointmentList;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Recipient> recepientList;

}
