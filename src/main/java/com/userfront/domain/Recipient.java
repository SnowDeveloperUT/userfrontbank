package com.userfront.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by snowwhite on 6/2/2017.
 */
@Data
@Entity
public class Recipient {
	@Id
	@GeneratedValue
	private long id;
	private String name;
	private String email;
	private String phone;
	private String accountNumber;
	private String description;

	@ManyToOne
	@JoinColumn(name="user_id")
	@JsonIgnore
	private User user;

}
