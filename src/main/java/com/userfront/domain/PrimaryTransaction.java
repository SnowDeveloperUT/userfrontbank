package com.userfront.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by snowwhite on 6/2/2017.
 */
@Data
@Entity
public class PrimaryTransaction {
	@Id
	@GeneratedValue
	private long id;
	private Date date;
	private String description;
	private String type;
	private String status;
	private String amount;
	private BigDecimal availableBalance; //Use bigDecimal as there will be lot of calculations so, we need for accuracy

	@ManyToOne
	@JoinColumn(name="primary_account_id")
	private PrimaryAccount primaryAccount;

	public PrimaryTransaction(Date date, String description, String type, String status, String amount, BigDecimal
			availableBalance, PrimaryAccount primaryAccount) {
		super();
		this.date = date;
		this.description = description;
		this.type = type;
		this.status = status;
		this.amount = amount;
		this.availableBalance = availableBalance;
		this.primaryAccount = primaryAccount;
	}


}
