package com.userfront.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by snowwhite on 6/2/2017.
 */
@Data
@Entity
public class PrimaryAccount {
	@Id
	@GeneratedValue
	private Long id;
	private int accountNumber;
	private BigDecimal accountBalance;

	@OneToMany(mappedBy = "primaryAccount", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	private List<PrimaryTransaction> primaryTransactionList;
}
