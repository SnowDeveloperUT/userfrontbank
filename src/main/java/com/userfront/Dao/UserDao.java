package com.userfront.Dao;

import com.userfront.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by snowwhite on 6/2/2017.
 */
public interface UserDao extends CrudRepository<User, Long>{
	User findByUsername(String username);
	User findByEmail(String email);


}
